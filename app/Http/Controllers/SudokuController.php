<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sudoku;

class SudokuController extends Controller
{
    public function index($sudoku = null)
    {
        $first = "009001080070380000800600035003100007085070306020930000050003910608290000300007200";

		$s1 = new Sudoku();

        $s1->create($first);

    	if(isset($sudoku))
    	{
    		$s1->create($sudoku);
    	}

        //  save history
        $save = Sudoku::where('numbers', $sudoku ?? $first)->limit(1);

        if($save->count() == 0) $s1->save();// else $save->touch();

        //  load history
        $history = Sudoku::limit(5)->orderBy('id', 'DESC')->get();
    	
    	$s111 = $s1->solve();

        //  is it valid
        $valid = $s1->valid;

    	return view('sudoku.index', compact('sudoku', 's111', 'valid', 'history'));
    }

    public function resetHistory()
    {
        Sudoku::all()->each(function ($value)
        {
            $value->delete();
        });

        return redirect('/sudoku');
    }
}
