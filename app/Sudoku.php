<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sudoku extends Model
{
    protected $guarded = [];

    public $fields = [];
    public $solved = [];
    public $numbers;
    public $valid = true;

    private $cn = [];

    public function __construct($sudoku = 0)
    {
    	if($sudoku != null) dd($sudoku);
    	for ($i=0; $i < 81; $i++) { 
    		$this->fields[] = 0;
    	}

    	if($sudoku != null) $this->create($sudoku);
    }

    public function echo()
    {
    	$color = "#4CAF50";
    	$result = "<table style='border: 4px solid $color'><tr>";

    	$n = collect($this->fields)->map(function ($value)
    	{
    		if($value == 0)
    			return '';
    		return $value;
    	});

    	for ($i=0; $i < 81; $i++) { 
    		if($i % 9 == 0 && $i != 0)
    		{
    			$result .= "</tr><tr>";
    		}

    		// field background
    		$background = "";
    		if($n[$i] == 0) $background = "background-color: silver; ";
    		if(array_search($i, $this->solved) !== false) $background = "background-color: lightgreen; ";

    		//	border
    		$border = "";
    		if($i % 3 === 0) $border = "border-left: 2px solid $color;";
    		if($i % 3 === 2) $border = "border-right: 2px solid $color;";
    		if((int)($i / 9) % 3 == 0) $border .= "border-top: 2px solid $color;";
    		if((int)($i / 9) % 3 == 2) $border .= "border-bottom: 2px solid $color;";

    		$result .= "<td width=50 height=50 style=\"$border $background\">" . $n[$i] . "</td>";
    	}

    	$result .= "</tr></table>";

    	return $result;
    }

    public function create($sudoku)
    {
    	$this->numbers = $sudoku;
    	$sudoku = str_split($sudoku);
    	$n = count($sudoku);
    	if($n < 81)
    	{
    		for ($i=$n; $i <= 81; $i++) { 
    			$sudoku[] = 0;
    		}
    	}

    	for ($i=0; $i < 81; $i++) { 
    		$this->fields[$i] = $sudoku[$i];
    	}

    	$this->fill([
	    'numbers' => $this->numbers]
	);

    	return $sudoku;
    }

    public function countNumbers()
    {
    	$this->cn = array_count_values($this->fields);

    	return $this->cn;
    }

    public function findRowColumn($index)
    {
    	$row = (int) ($index / 9);
    	$column = $index % 9;

    	return [$row, $column];
    }

    public function findQuadrant($row, $column)
    {
    	return (int)($row / 3) * 3 + (int)($column / 3) + 1;
    }

    public function findQuadrantByIndex($index)
    {
    	return (int)((int) ($index / 9) / 3) * 3 + (int)(($index % 9) / 3) + 1;
    }

    public function searchQuadrant($n, $quadrant, $index)
    {
    	$values = $this->readQuadrant($quadrant);

    	$values1 = collect($values)->map(function ($value)
    	{
    		if($value[1] !== 0) return $value[1];
    	})->reject(function ($value, $key)
    	{
    		return $value == 0;
    	})->toArray();

    	//	check quadrant for duplicates
		if(count(array_unique($values1)) < count($values1)) $this->valid = false;

    	$xy = $this->numberLocationInOtherQuadrants($quadrant, $index);

    	$a = 123;

    	for ($i=0; $i < count($values); $i++) 
    	{

    		if($values[$i][1] == 0)
    		{
    			$a = $this->findRowColumn($values[$i][0]);

    			if (array_search($a[0], $xy[0]) !== false) {
    				$values[$i][1] = 1;
    			}
    			if (array_search($a[1], $xy[1]) !== false) {
    				$values[$i][1] = 1;
    			}
    		}
    	}

		$count = 0;
		$location = -1;

		foreach ($values as $value) {
			if($value[1] == 0) { $count++; $location = $value[0]; }
		}

		if ($count == 1) {
			$this->fields[$location] = $n;
			$this->solved[] = $location;
		}

    	return $values;
    }

    public function readQuadrant($quadrant)
    {
    	$result = [];

    	$row = (int)(($quadrant - 1) / 3) * 3;
    	$column = (($quadrant % 3) - 1) * 3;

    	for ($i=0; $i < 81; $i++) { 
    		$number = $this->findQuadrantByIndex($i);

    		if($number == $quadrant) $result[] = [$i, $this->fields[$i]];
    	}

    	return $result;
    }

    public function numberLocationInOtherQuadrants($quadrant, $index)
    {
    	$rows = [];
    	$columns = [];

    	foreach ($index as $n) {
    		$xy = $this->findRowColumn($n);
    		$nq = $this->findQuadrant($xy[0], $xy[1]);
    		if((int)(($nq - 1) / 3) == (int)(($quadrant - 1) / 3))
    		{
    			if(array_search($xy[0], $rows) !== false) $this->valid = false; else $rows[] = $xy[0];
    		}
    		if($nq % 3 == $quadrant % 3)
    		{
    			if(array_search($xy[1], $columns) !== false) $this->valid = false; else $columns[] = $xy[1];
    		}
    	}

    	return [$rows, $columns];
    }

    public function solveByNumbers()
    {
    	if($this->cn == []) $this->countNumbers();

    	$cn = $this->cn;

    	//	delete empty fields
    	unset($cn['0']);

    	//	sort by highest occurence of the numbers in a game
    	arsort($cn);

    	while (count($cn) > 0) {
	    	//	gets the first highest number
	    	$n = array_key_first($cn);

	    	//	find all occurences of that number
	    	$index = array_keys($this->fields, $n);

	    	$xy = [];
	    	$quadrant = [];

	    	// dd($cn);

	    	for ($i=0; $i < count($index); $i++) { 
	    		$xy[] = $this->findRowColumn($index[$i]);

				$quadrant[] = $this->findQuadrant($xy[$i][0], $xy[$i][1]);
	    	}

	    	$freeQuadrants = array_values(array_diff([1, 2, 3, 4, 5, 6, 7, 8, 9], $quadrant));

	    	foreach ($freeQuadrants as $key) {
	    		$this->searchQuadrant($n, $key, $index);
	    	}

	    	unset($cn[$n]);
    	}


    	
    }

    public function solveByQuadrants()
    {
    	for ($i=1; $i < 10; $i++) { 
    		$a = $this->readQuadrant($i);
    		$values = [];
    		$count = 0;
    		$location = -1;

    		foreach ($a as $a1) {
    			$values[] = $a1[1];
    			if($a1[1] == 0) { $count++; $location = $a1[0]; }
    		}

    		if($count == 1)
    		{
    			$array = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    			$result = array_diff($array, $values);

    			$this->fields[$location] = reset($result);
    			$this->solved[] = $location;
    		}
    	}
    }

    public function solveByRowsColmuns()
    {
    	return true;
    }

    public function solve()
    {
    	$solved = 0;

    	do {
    		$solved = count($this->solved);

    		$this->solveByNumbers();
    	} while ($solved < count($this->solved));

    	do {
    		$solved = count($this->solved);

    		$this->solveByQuadrants();
    	} while ($solved < count($this->solved));

    	if(!isset($this->countNumbers()[0])) $this->done = true;

    	return $this->echo();// . print_r($this->solved, true) . "<br>" . print_r($index ?? '', true);
    }

}
