#!/bin/sh
# activate maintenance mode
cd /var/www/html/upwork
sudo su
php artisan down
# update source code
php artisan migrate:fresh

git reset --hard
git pull
# update PHP dependencies
export HOME=/root && composer install --no-interaction --no-dev --prefer-dist
# --no-interaction Do not ask any interactive question
# --no-dev  Disables installation of require-dev packages.
# --prefer-dist  Forces installation from package dist even for dev versions.
# update database
# php artisan migrate --force
# --force  Required to run when in production.

chmod +x deploy.sh
chown -R www-data:www-data /var/www/html/upwork

# stop maintenance mode

php artisan up