@extends('layouts.app')

@section('style')
td { border: 1px solid #dcdcdc; text-align: center; font-size: 14px; font-weight: bold; }
@endsection

@section('content')
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content"><center>
        Sudoku Solver

        <br>

            <br>
            <form onsubmit="myFunction(); return false;">
                Sudoku numbers: <input type="text" name="sudoku" value="{{ $sudoku ?? '' }}">
                <input type="submit" name="submit">
            </form>


            <br>
        White fields are starting numbers, green are solved fields and rest of fields are silver, non-solved ones.

            <br>
            <br>

        {!! $s111 ?? '' !!}

        @if($valid) <p>It is valid.</p> @else <p>It is not valid.</p> @endif
        <br>
        History <br>
        @foreach($history as $a)
            <a href="/sudoku/{{ json_decode($a)->numbers }}">#{{ $loop->iteration }}</a> <br>
        @endforeach

        <br>

        <a href="/sudoku/resetHistory">Reset history</a>

        <script type="text/javascript">
            function myFunction() {
                var sudoku = document.getElementsByName('sudoku')[0];
                if(sudoku.value == "") return false;
                window.location = '/sudoku/' + sudoku.value;
            }
        </script>
    </div>
</center></div>
@endsection