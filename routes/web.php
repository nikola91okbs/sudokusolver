<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sudoku/{sudoku?}', 'SudokuController@index')->where('sudoku', '[0-9]+');

Route::post('/deploy', 'DeployController@deploy');

Route::get('/sudoku/resetHistory', 'SudokuController@resetHistory');